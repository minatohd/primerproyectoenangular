import { Component } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: 'body.component.html'
})
export class BodyComponent {
  mostrar:boolean = false;
  frase:any = {
    mensaje: "Prueba",
    autor: "Edu"
  }

  personajes:string[] = ["Spoderman", "Venom", "Dr.Octopus"];

}
