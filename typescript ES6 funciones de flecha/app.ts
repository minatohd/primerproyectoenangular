let miFuncion = function ( a ){
  return a;
}

let miFuncionF = a => a;

let miFuncion2 = function ( a:number, b:number ){
  return a + b;
}

let miFuncion2F = ( a:number, b:number ) => a + b;

console.log(miFuncion("Normal"));
console.log(miFuncionF("Flecha"));
console.log(miFuncion2(1,2));
console.log(miFuncion2F(1,4));
