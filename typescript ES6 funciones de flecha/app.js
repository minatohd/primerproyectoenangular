"use strict";
var miFuncion = function (a) {
    return a;
};
var miFuncionF = function (a) { return a; };
var miFuncion2 = function (a, b) {
    return a + b;
};
var miFuncion2F = function (a, b) { return a + b; };
console.log(miFuncion("Normal"));
console.log(miFuncionF("Flecha"));
console.log(miFuncion2(1, 2));
console.log(miFuncion2F(1, 4));
