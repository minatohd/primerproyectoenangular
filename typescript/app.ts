interface tipotest{
  nombre:string,
  poder:string
}

function enviarMision(variable:tipotest){
    console.log("enviar a:" + variable.nombre);
}

let datos:tipotest = {
  nombre: "Edu",
  poder: "angular"
};

enviarMision(datos);


// como hacer una clase

class pruebaClase {
  name:string = "prueba";
  lastname:string;
  active:boolean;
  edad:number;
  constructor(){
    console.log("se ejecuto el constructor");
  }
}

let prueba:pruebaClase = new pruebaClase();
console.log(prueba);
