"use strict";
function enviarMision(variable) {
    console.log("enviar a:" + variable.nombre);
}
var datos = {
    nombre: "Edu",
    poder: "angular"
};
enviarMision(datos);
// como hacer una clase
var pruebaClase = /** @class */ (function () {
    function pruebaClase() {
        this.name = "prueba";
        console.log("se ejecuto el constructor");
    }
    return pruebaClase;
}());
var prueba = new pruebaClase();
console.log(prueba);
