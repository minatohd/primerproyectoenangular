"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var xmen = /** @class */ (function () {
    function xmen(nombre, clave) {
        this.nombre = nombre;
        this.clave = clave;
    }
    xmen.prototype.imprimir = function () {
        console.log(this.nombre + " - " + this.clave);
    };
    return xmen;
}());
exports.xmen = xmen;
