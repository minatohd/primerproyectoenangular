function activar( quien:string, objeto:string = "funcion", momento?:string ){

    let mensaje:string;

    if (momento){
      mensaje = `${quien} activo la ${objeto} en ${momento}`;
    }else{
      mensaje = `${quien} activo la ${objeto}`;
    }
    console.log(mensaje);

}

activar("Gordon","test","10minutos");
