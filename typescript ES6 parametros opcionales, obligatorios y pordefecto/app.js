"use strict";
function activar(quien, objeto, momento) {
    if (objeto === void 0) { objeto = "funcion"; }
    var mensaje;
    if (momento) {
        mensaje = quien + " activo la " + objeto + " en " + momento;
    }
    else {
        mensaje = quien + " activo la " + objeto;
    }
    console.log(mensaje);
}
activar("Gordon", "test", "10minutos");
